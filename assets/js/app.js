+function() {
  'use strict';
  var app = new Marionette.Application();

  app.addRegions( {
    mainRegion: '#main-region',
    dialogRegion: '#dialog-region'
  });

  app.navigate = function(route, options) {
    options || (options = {});
    Backbone.history.navigate(route, options);
  };

  app.getCurrentRoute = function() {
    return Backbone.history.fragment;
  };

  app.on('start', function() {
    if (Backbone.history) {
      Backbone.history.start();

      if (this.getCurrentRoute() === '') {
        app.trigger('contacts:list');
      }
    }
  });

  window.ContactManager = app;
}();