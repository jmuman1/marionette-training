+function() {
  'use strict';

  ContactManager.module('ContactsApp.List', function(List, app, Backbone, Marionette, $, _) {
    List.Controller = {
      listContacts: function() {
        var loadingView = new app.Common.Views.Loading();
        app.mainRegion.show(loadingView);

        var fetchingContacts = app.request('contact:entities');
        $.when(fetchingContacts).done(function(contacts) {
          var contactsListView = new List.Contacts({
            collection: contacts
          });

          contactsListView.on('childview:contact:delete', function(childView, model) {
            model.destroy();
          });

          contactsListView.on('childview:contact:show', function(childView, model) {
            app.trigger('contact:show', model.get('id'));
          });

          contactsListView.on('childview:contact:edit', function(childView, model) {
            console.log('edit link clicked');
          });

          app.mainRegion.show(contactsListView);
        });
      }
    };
  });
}();