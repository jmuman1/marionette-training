+function() {
  ContactManager.module('ContactsApp.Show', function(Show, app, Backbone, Marionette, $, _) {
    Show.Controller = {
      showContact: function(id) {
        var loadingView = new app.Common.Views.Loading({
          title: 'Artificial Loading Delay',
          message: 'Data loading is delayed to demonstrate using a loading view.'
        });
        app.mainRegion.show(loadingView);

        var fetchingContact = app.request('contact:entity', id);

        $.when(fetchingContact).done(function(contact){
          var contactView;

          if (contact !== undefined) {
            contactView = new Show.Contact({
              model: contact
            });

            contactView.on('contact:edit', function(contact) {
              app.trigger('contact:edit', contact.get('id'));
            });
          } else {
            contactView = new Show.MissingContact();
          }

          app.mainRegion.show(contactView);
        });
      }
    };
  });
}();