+function() {
  'use strict';

  ContactManager.module('ContactsApp.Edit', function(Edit, app, Backbone, Marionette, $, _){
    Edit.Controller = {
      editContact: function(id) {
        var loadingView = new app.Common.Views.Loading({
          title: 'Artificial Loading Delay',
          message: 'Data loading is delayed to demonstrate using a loading view.'
        });

        app.mainRegion.show(loadingView);

        var fetchingContact = app.request('contact:entity', id);
        $.when(fetchingContact).done(function(contact) {
          var view;
          if (contact !== undefined) {
            view = new Edit.Contact({
              model: contact
            });

            view.on('form:submit', function(data) {
              if (contact.save(data)) {
                app.trigger('contact:show', contact.get('id'));
              } else {
                view.triggerMethod('form:data:invalid', contact.validationError);
              }
            })
          } else {
            view = new app.ContactsApp.Show.MissingContact();
          }
          app.mainRegion.show(view);
        });
      }
    };
  });
}();