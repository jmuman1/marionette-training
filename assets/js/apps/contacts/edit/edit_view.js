+function() {
  'use strict';

  ContactManager.module('ContactsApp.Edit', function(Edit, app, Backbone, Marionette, $, _) {
    Edit.Contact = Marionette.ItemView.extend({
      template: '#contact-form',

      events: {
        'click button.js-submit': 'submitClicked'
      },

      submitClicked: function(e) {
        e.preventDefault();
        var data = Backbone.Syphon.serialize(this);
        this.trigger('form:submit', data);
      },

      onFormDataInvalid: function(errors) {
        var $view = this.$el;
        var clearFormErrors = function() {
          var $form = $view.find('form');
          $form.find('.help-block.error').each(function() {
            $(this).remove();
          });
          $form.find('.form-group.has-error').each(function(){
            $(this).removeClass('has-feedback has-error');
          });
        };

        var markErrors = function(value, key) {
          var $controlGroup = $view.find('#contact-' + key).parent();
          var $errorIcon = $('<span>', {class:'glyphicon glyphicon-remove form-control-feedback'});
          var $errorEl = $('<span>', {class: 'help-block error', text: value});
          $controlGroup.addClass('has-feedback has-error')
            .append($errorIcon)
            .append($errorEl);
        };
        clearFormErrors();
        _.each(errors, markErrors);
      }
    });
  });
}();