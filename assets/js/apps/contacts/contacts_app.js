+function() {
  'use strict';

  ContactManager.module('ContactsApp', function(ContactsApp, app, Backbone, Marionette, $, _) {
    app.Router = Marionette.AppRouter.extend({
      appRoutes: {
        'contacts': 'listContacts',
        'contacts/:id': 'showContact',
        'contacts/:id/edit': 'editContact'
      }
    });

    var API = {
      listContacts: function() {
        ContactsApp.List.Controller.listContacts();
      },
      showContact: function(id) {
        ContactsApp.Show.Controller.showContact(id);
      },
      editContact: function(id) {
        ContactsApp.Edit.Controller.editContact(id);
      }
    };

    app.on('contacts:list', function() {
      app.navigate('contacts');
      API.listContacts();
    });

    app.on('contact:show', function(id) {
      app.navigate('contacts/' + id);
      API.showContact(id);
    });

    app.on('contact:edit', function(id){
      app.navigate('contacts/' + id + '/edit');
      API.editContact(id);
    })

    app.addInitializer(function() {
      new app.Router({
        controller: API
      });
    });
  });
}();